#!/bin/sh
# NEEDS glib2 installed to be able to do configuration.
# https://packages.debian.org/source/jessie/glib2.0
sudo apt-get install libglib2.0-dev
sudo apt-get install libglib2.0-0
sudo apt-get install binutils
sudo apt-get install gcc-arm-linux-gnueabihf 
sudo apt-get install g++-6-arm-linux-gnueabihf

mkdir v8
cd v8
fetch v8
echo "target_os = ['android']" >> .gclient 
gclient sync
./build/install-build-deps.sh
alias gm=~/v8/v8/tools/dev/gm.py
cd v8
# start buildingS A)
gm android_arm.release.check

## OR B) this is the way I more or less use it
## for armv8, target_cpu must be "arm64"
#target_cpu = "arm64"
#v8_target_cpu = "arm64"
# taken from https://github.com/cmdr2/v8-android
tools/dev/v8gen.py gen -m client.v8.ports -b "V8 Android Arm - builder" android_arm.release
gn args out.gn/android_arm.release
# use these flags:
android_unstripped_runtime_outputs = false
is_component_build = false
is_debug = false
symbol_level = 1
target_cpu = "arm"
target_os = "android"
use_goma = false
v8_android_log_stdout = true
v8_static_library = true
use_custom_libcxx = false
use_custom_libcxx_for_host = false
v8_use_external_startup_data = false
# Next, run 
ninja -C out.gn/android_arm.release
# create lib
mkdir libs
cd libs
ar -rcsD libv8_base.a ../out.gn/android_arm.release/obj/v8_base/*.o
ar -rcsD libv8_base.a ../out.gn/android_arm.release/obj/v8_libbase/*.o
ar -rcsD libv8_base.a ../out.gn/android_arm.release/obj/v8_libsampler/*.o
ar -rcsD libv8_base.a ../out.gn/android_arm.release/obj/v8_libplatform/*.o
ar -rcsD libv8_base.a ../out.gn/android_arm.release/obj/src/inspector/inspector/*.o
ar -rcsD libv8_base.a ../out.gn/android_arm.release/obj/third_party/icu/icuuc/*.o
ar -rcsD libv8_base.a ../out.gn/android_arm.release/obj/third_party/icu/icui18n/*.o
ar -rcsD libv8_snapshot.a ../out.gn/android_arm.release/obj/v8_snapshot/*.o
# copy includes
cp -R /path/to/v8/include .

# package the whole ordeal
tar -zcvf v8_android_arm32.tar.gz libs

#Either create a new Android NDK project in your Android Studio, or use one that's already set up with CMake
#Copy the include directory from v8, and the static libraries created previously and paste into an Android project's app/libs/armeabi-v7a/ directory.
#Specify these libraries and include directory path in the Android project's CMakesLists.txt file.
#Use v8 in the native C++ file, which gets called from the Java Android Activity via JNI.
#Use the example project for the example CMakeLists.txt and C++ file.







##### OR C)
# taken from https://v8.dev/docs/cross-compile-arm
tools/dev/v8gen.py arm.release
#Then run 
gn args out.gn/arm.release 
#and make sure you have the following keys:
target_os = "android"      # These lines need to be changed manually
target_cpu = "arm"         # as v8gen.py assumes a simulator build.
v8_target_cpu = "arm"
is_component_build = false
#The keys should be the same for debug builds. If you are building for an arm64 device like the Pixel C, 
#which supports 32bit and 64bit binaries, the keys should look like this:
target_os = "android"      # These lines need to be changed manually
target_cpu = "arm64"       # as v8gen.py assumes a simulator build.
v8_target_cpu = "arm64"
is_component_build = false
#Now build:
ninja -C out.gn/arm.release d8
#Use adb to copy the binary and snapshot files to the phone:

adb shell 'mkdir -p /data/local/tmp/v8/bin'
adb push out.gn/arm.release/d8 /data/local/tmp/v8/bin
adb push out.gn/arm.release/icudtl.dat /data/local/tmp/v8/bin
adb push out.gn/arm.release/natives_blob.bin /data/local/tmp/v8/bin
adb push out.gn/arm.release/snapshot_blob.bin /data/local/tmp/v8/bin
rebuffat:~/src/v8$ adb shell
bullhead:/ $ cd /data/local/tmp/v8/bin
bullhead:/data/local/tmp/v8/bin $ ls
v8 natives_blob.bin snapshot_blob.bin
bullhead:/data/local/tmp/v8/bin $ ./d8
#V8 version 5.8.0 (candidate)
#d8> 'w00t!'
#"w00t!"
#d8>