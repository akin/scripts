#!/bin/sh
apt-get install git
apt-get install vim
apt-get install screen
apt-get install sudo

#git setup
git config --global user.name "John Doe"
git config --global user.email "jdoe@email.com"
git config --global core.autocrlf false
git config --global core.filemode false
# and for fun!
git config --global color.ui true

git clone https://akin@bitbucket.org/akin/scripts.git

echo "to add sudoer:"
echo "adduser <username> sudo"